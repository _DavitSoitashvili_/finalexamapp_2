package davit.soitashvili.demo.dto_mappers;

import davit.soitashvili.demo.models.Task;
import davit.soitashvili.demo.models.dtos.TaskDto;

import java.util.Date;

public class Mappers {

    public static Task mapToTask(TaskDto taskDto) {
        return new Task(taskDto.getTitle(), taskDto.getDescription(), new Date() , taskDto.getEndDate());
    }
}
