package davit.soitashvili.demo.services;

import davit.soitashvili.demo.dto_mappers.Mappers;
import davit.soitashvili.demo.models.Task;
import davit.soitashvili.demo.models.dtos.TaskDto;
import davit.soitashvili.demo.repositories.TasksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TasksServiceImpl implements TasksService {

    @Autowired
    TasksRepository tasksRepository;

    @Override
    public List<Task> getAllTasks() {
        return tasksRepository.findAll();
    }

    @Override
    public void addTask(TaskDto taskDto) {
        tasksRepository.save(Mappers.mapToTask(taskDto));
    }

    @Override
    public void deleteTask(Long id) {
        tasksRepository.deleteById(id);
    }
}
