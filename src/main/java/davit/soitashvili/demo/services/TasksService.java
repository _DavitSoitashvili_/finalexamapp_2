package davit.soitashvili.demo.services;

import davit.soitashvili.demo.models.Task;
import davit.soitashvili.demo.models.dtos.TaskDto;

import java.util.List;

public interface TasksService {
    List<Task> getAllTasks();

    void addTask(TaskDto taskDto);

    void deleteTask(Long id);
}
