package davit.soitashvili.demo.repositories;

import davit.soitashvili.demo.models.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TasksRepository extends JpaRepository<Task, Long> { }
