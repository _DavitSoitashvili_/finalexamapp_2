package davit.soitashvili.demo.controllers;

import davit.soitashvili.demo.models.Task;
import davit.soitashvili.demo.models.dtos.TaskDto;
import davit.soitashvili.demo.services.TasksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("api/")
public class TasksController {
    @Autowired
    TasksService tasksService;

    @GetMapping("/tasks")
    public List<Task> getAllTasks() {
        return tasksService.getAllTasks();
    }

    @PostMapping("/add-task")
    public void addTask(@RequestBody TaskDto task) {
        tasksService.addTask(task);
    }

    @DeleteMapping("/remove-task{id}")
    public void removeTask(@PathVariable long id) {
        tasksService.deleteTask(id);
    }
}
